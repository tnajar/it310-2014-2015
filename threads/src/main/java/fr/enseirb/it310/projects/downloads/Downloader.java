package fr.enseirb.it310.projects.downloads;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class Downloader {
    public static void main(String[] args) {
        List<URL> urls = buildURLs(args);
        
        Downloader d = new Downloader();
        d.download(urls);
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        
        String action = null;
        while (!d.downloadsFinished()) {
            System.out.print("Press S to stop download: ");
            try {
                action = r.readLine();
                if ("S".equalsIgnoreCase(action)) {
                    System.out.println("Stopping downloads");
                    d.stop();
                    System.out.println("Downloads stopped");
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void stop() {
        // TODO
    }

    private boolean downloadsFinished() {
        // TODO
        return false;
    }

    public void download(List<URL> urls) {
        // TODO
    }

    private static List<URL> buildURLs(String[] urlsAsString) {
        List<URL> urls = new LinkedList<URL>();
        
        for (String u : urlsAsString) {
            try {
                urls.add(new URL(u));
            } catch (MalformedURLException e) {
                System.err.println(u + " is not a valid URL");
            }
        }
        return urls;
    }
}
