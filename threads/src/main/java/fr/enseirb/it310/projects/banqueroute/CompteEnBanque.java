package fr.enseirb.it310.projects.banqueroute;

public class CompteEnBanque {
    private int solde = 0;
    
    public CompteEnBanque(int soldeInitial) {
        solde = soldeInitial;
    }
    
    public int getSolde() {
        return solde;
    }
    
    public void retireUn() {
        solde--;
    }
}
